/**
 * @author Vincent Vatelot pour Alice et l'assocation Nos tout petits
 * @author Sébastien Lecocq
 * @since Décembre 2017
 */

$(window).resize(function () {
    location.reload();
});

function animateBallon(id) {
    var x = $(document).width() * Math.random();
    var y = $(document).height() + 355;
    var elevate = y + 355;

    var minDelay = 7000;
    var delay = minDelay + Math.random() * minDelay * 1.5;
    console.log(delay);
    
    $("<img>", {
        id: id,
        class: "ballon",
        src: "./images/ballon" + id + ".png"
    })
        .css({
            "position": 'absolute',
            "z-index": 0
        })
        .offset({
            top: y,
            left: x,
        })
        .appendTo("#cloud-intro")
        .animate({
            top: '-=' + elevate + 'px'
        }, delay * 5, 'linear', function () {
            $("#" + id).remove();
            animateBallon(id);
        });
}

function mainMove() {
    for (i = 1; i <= 3; i++) {
        animateBallon(i);
    }
}

mainMove();

var options = {
    textFont: null,
    textColour: null,
    weight: true,
    initial: [0.220, -0.040],
    minSpeed: 0.005,
    maxSpeed: 0.007,
    shadowBlur: 0,
    depth: 0.8,
    outlineMethod: "none",
    shuffleTags: true,
    reverse: true
},
    WebFontConfig = {
        google: {
            families: [
                'Poiret+One::latin',
                'Architects+Daughter::latin',
                'Oxygen::latin',
                'Indie+Flower::latin'
            ]
        },
        active: function () {
            try {
                TagCanvas.Start('myCanvas', 'tags', options);

                var i = 0;
                setInterval(function () {
                    i++;
                    rotation(i / 8);
                }, 15000);

                $('a').click(function () {
                    return false;
                });
            } catch (e) {
                // something went wrong, hide the canvas container
                document.getElementById('myCanvasContainer').style.display = 'none';
            }
        }
    };

function rotation(stepAngle) {
    var speed = 0.002;
    TagCanvas.tc.myCanvas.pitch = Math.sin(Math.PI * stepAngle) * speed;
    TagCanvas.tc.myCanvas.yaw = Math.cos(Math.PI * stepAngle) * speed;
}

(function () {
    $.each(prenoms, function (index) {
        var police = polices[Math.floor(Math.random() * 3)];
        $("#tags ul").append(
            $("<li>", {}).append(
                $("<a>", {}).text(
                    prenoms[index][0]
                ).css({ "font-family": police, "color": couleurs[prenoms[index][1]] })
            )
        );
    });

    var wf = document.createElement('script');
    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
        '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
    wf.type = 'text/javascript';
    wf.async = 'true';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(wf, s);
})();